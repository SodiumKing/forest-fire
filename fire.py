from tkinter import *
#import datetime
import random
#import sys
import argparse

master = Tk()
canvas = Canvas(master,width = 5 , height = 5)

parser = argparse.ArgumentParser()
parser.add_argument("-nbligne",type =int,help="Nb ligne",default=3)
parser.add_argument("-nbcol",type =int,help="Nb col",default=3)
parser.add_argument("-taille_cell",type =int,help="taille_cell",default=20)
parser.add_argument("-proba",type=float,help="Proba foret en %",default=.6)
args = parser.parse_args()


def create_forest(nbcol,nbligne,taille_cell,proba):

    x1 = 0
    y1 = 0
    x2 = taille_cell
    y2 = taille_cell
    nb_col=0
    coor_x1 = x1
    coor_x2 = x2
    
    for nb_col in range(nbcol):
                coor_x1 += taille_cell
                coor_x2 += taille_cell
                coor_y1 = y1
                coor_y2 = y2
                nb_row =0

                for nb_row in range(nbligne):
                    random_number = random.random()

                    if random_number <= proba:
                        color = "green"
                    else:
                        color="white"
                    if nb_row == 0:
                        canvas.create_rectangle(coor_x1 ,coor_y1 ,coor_x2 ,coor_y2 ,fill=color,tag="dessus")
                    elif nb_row == nbligne -1:
                        canvas.create_rectangle(coor_x1 ,coor_y1 ,coor_x2 ,coor_y2 ,fill=color,tag="dessous")
                    else:
                        canvas.create_rectangle(coor_x1 ,coor_y1 ,coor_x2 ,coor_y2 ,fill=color,tag="rectangle")

                    coor_y1 += taille_cell
                    coor_y2 += taille_cell
                    nb_row +=1
                nb_col = nb_col +1
                
    canvas.pack()


def click_callback(event):
    x1 = event.x
    y1 = event.y
    canvas = event.widget
    plus_proche = canvas.find_closest(x1,y1)
    couleur_actuelle = canvas.itemcget(plus_proche,'fill')
    if couleur_actuelle == 'green':
        canvas.itemconfig(plus_proche[0],fill='red')




def regenerer():
    create_forest(args.nbligne,args.nbcol,args.taille_cell,args.proba / 100)

def regle1():
    feu = []
    cendre = []

    for rectangle in canvas.find_all():
        if canvas.itemcget(rectangle,'fill') == 'red':
            feu.append(rectangle)

        if canvas.itemcget(rectangle,'fill') == 'gray':
            cendre.append(rectangle)

    for rectangle in feu:
        tag = canvas.gettags(rectangle)

        if  tag[0] == 'dessous':

            if canvas.itemcget(rectangle+args.nbligne,'fill') == 'green':
                canvas.itemconfig(rectangle+args.nbligne, fill='red')

            if canvas.itemcget(rectangle-args.nbligne,'fill') == 'green':
                canvas.itemconfig(rectangle-args.nbligne, fill='red')

            if canvas.itemcget(rectangle-1,'fill') == 'green':
                canvas.itemconfig(rectangle-1, fill='red')


        elif tag[0] == 'dessus':

            if canvas.itemcget(rectangle+args.nbligne,'fill') == 'green':
                canvas.itemconfig(rectangle+args.nbligne, fill='red')

            if canvas.itemcget(rectangle-args.nbligne,'fill') == 'green':
                canvas.itemconfig(rectangle-args.nbligne, fill='red')

            if canvas.itemcget(rectangle+1,'fill') == 'green':
                canvas.itemconfig(rectangle + 1, fill='red')

        else:

            if canvas.itemcget(rectangle+args.nbligne,'fill') == 'green':
                canvas.itemconfig(rectangle+args.nbligne, fill='red')

            if canvas.itemcget(rectangle-args.nbligne,'fill') == 'green':
                canvas.itemconfig(rectangle-args.nbligne, fill='red')

            if canvas.itemcget(rectangle+1,'fill') == 'green':
                canvas.itemconfig(rectangle + 1, fill='red')

            if canvas.itemcget(rectangle-1,'fill') == 'green':
                canvas.itemconfig(rectangle-1, fill='red')
                
        canvas.itemconfig(rectangle,fill='gray')

    for rectangle in cendre:
        canvas.itemconfig(rectangle,fill='white')

    master.after(1000,regle1)    


def selectregle1():
    button_simulation.config(command=regle1)




master.title("Feu de Foret")

canvas = Canvas(master,height=(args.nbligne+1)*args.taille_cell,width=(args.nbcol+1)*args.taille_cell)

menu = Tk()
menu.title("Menu")
menu.rowconfigure(0,weight=1,minsize=80)
menu.rowconfigure(2,weight=1,minsize=80)
menu.rowconfigure(4,weight=1,minsize=80)


button_Feu = Button(menu, text="     Feu\t",background='red',highlightbackground='blue',highlightcolor='green',highlightthickness='10' ,command=canvas.bind('<Button-1>', click_callback))
button_Feu.grid(column=0,row=1)

button_regenerer = Button(menu, text="Regenerer",highlightbackground='blue',highlightcolor='green',highlightthickness='10' ,command=regenerer)
button_regenerer.grid(column=4,row=1)
button_simulation = Button(menu,text="Lancez la simulation")
button_simulation.grid(column=2,row=3)



radio_button_regle1 = Radiobutton(menu,text="Regle 1",value=1,command=selectregle1)
radio_button_regle1.grid(column=0,row=5)

create_forest(args.nbligne,args.nbcol,args.taille_cell,args.proba / 100)

menu.mainloop()
master.mainloop()